#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>
#include <time.h>

#include "arduino-serial-lib.h"

#define CANT 12

float calculateSD(float data[], int len);
float media(float data[], int len);
float minimo(float data[], int len);
float maximo(float data[], int len);
float aleatorio();

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
	srand (time(NULL));
	int fd = -1;
	int baudrate = 9600;  // default
	
	int buf = 0;
	int buf2 = 0;
	int flag;

	float alea = 0.0;
	float datosTemperatura[CANT];
	float minTemperatura = 0.0;
	float maxTemperatura = 0.0;
	float medTemperatura = 0.0;
	float sdTemperatura = 0.0;
	//----------------------------
	float alea2 = 0.0;
	float datosHumedad[CANT];
	float minHumedad = 0.0;
	float maxHumedad = 0.0;
	float medHumedad = 0.0;
	float sdHumedad = 0.0;
	//----------------------------
	int i=0;

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;

	}else {
        printf("-> Conectado\n");
        printf("-> Presione Ctrl + C para terminar la ejecucion <-\n");
		flag = 0;

		serialport_flush(fd);
		
		while (flag!=1) {
			
			// Codigo para medir la temperatura
		 	write(fd, "t", 1);
		    read(fd, &buf, sizeof(int));
			// Codigo para medir la humedad
		 	write(fd, "h", 1);
		    read(fd, &buf2, sizeof(int));
			
			alea = (float)buf;		// Temperatura
			alea2 = (float)buf2;    // Humedad

		    datosTemperatura[i] = alea;
		    datosHumedad[i] = alea2;

		    if ( i < CANT-1 ){
		    	
		    	printf("%d seg -> Temperatura: %6.3f \t Humedad: %6.3f\n", 5*(i+1), alea, alea2);

		    }
			else {

		    	printf("%d seg -> Temperatura: %6.3f \t Humedad: %6.3f", 5*(i+1), alea, alea2);

				sdTemperatura = calculateSD(datosTemperatura, CANT);
				sdHumedad = calculateSD(datosHumedad, CANT);
				minTemperatura = minimo(datosTemperatura, CANT);
				minHumedad = minimo(datosHumedad, CANT);
				maxTemperatura = maximo(datosTemperatura, CANT);
				maxHumedad = maximo(datosHumedad, CANT);
				medTemperatura = media(datosTemperatura, CANT);
				medHumedad = media(datosHumedad, CANT);

				printf("\n\n");
				printf("\n  >     SD Temperatura = %6.3f", sdTemperatura);
				printf("\n  >    Min Temperatura = %6.3f", minTemperatura);
				printf("\n  >    Max Temperatura = %6.3f", maxTemperatura);
				printf("\n  >  Media Temperatura = %6.3f", medTemperatura);
				printf("\n");
				printf("\n  >     SD Humedad = %6.3f", sdHumedad);
				printf("\n  >    Min Humedad = %6.3f", minHumedad);
				printf("\n  >    Max Humedad = %6.3f", maxHumedad);
				printf("\n  >  Media Humedad = %6.3f", medHumedad);
				printf("\n\n");

				// Reset arreglo
				for (int i = 0; i < 12; ++i){
					datosTemperatura[i] = 0.0;
				}
				sdTemperatura = 0.0;
				sdHumedad = 0.0;
				i = -1;
			}
			sleep(5);
			i++;

		}
	}
	
	close( fd );
	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[], int len) {
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < len; ++i) {
        sum += data[i];
    }

    mean = sum/len;

    for(i = 0; i < len; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / len);
}



/* Ejemplo para calcular la media */
float media(float data[], int len) {
    float sum = 0.0, mean, standardDeviation = 0.0;
    int i;

    for(i = 0; i < len; ++i) {
        sum += data[i];
    }

    mean = sum/len;

    return mean;
}



/* Funcion para hallar el valor minimo de un arreglo */
float minimo(float data[], int len) {
    float min;

    min = data[0];
    for(int i = 0; i < len; i++) {
        if ( data[i] < min ){
        	min = data[i];
        }
    }

    return min;
}



/* Funcion para hallar el valor maximo de un arreglo */
float maximo(float data[], int len) {
    float max;

    max = data[0];
    for(int i = 0; i < len; i++) {
        if ( data[i] > max ){
        	max = data[i];
        }
    }

    return max;
}



// Funcion para simular el arduino
float aleatorio(){
	int numero;
	int P = 23;
	int U = 26;
	// Este está entre M y N
	numero = P+(int)(((U-P+1.0)*rand())/(RAND_MAX+1.0));
	return (float)numero;
}